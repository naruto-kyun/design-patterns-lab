package eu.telecomnancy.sensor;

public class SensorCommandGetValue extends SensorCommand {	
	@Override
	void execute() {
		try {
			System.out.println("Sensor value : " +this.sensor.getValue());
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}

}
