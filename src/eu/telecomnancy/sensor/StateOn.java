package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn implements State {

	@Override
	public boolean getState() {
		return true;
	}

	@Override
	public double getValue() {
		return (new Random()).nextDouble() * 100;
	}
	
}
