package eu.telecomnancy.sensor;

public abstract class SensorCommand {
	protected ISensor sensor;
	
	public void setSensor(ISensor sensor){
		this.sensor = sensor;
	}
	
	abstract void execute();
}
