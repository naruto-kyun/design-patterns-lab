package eu.telecomnancy.sensor;

public class SensorDecoratorFahrenheit extends SensorDecorator {
	public SensorDecoratorFahrenheit(ISensor _sensor){
		super(_sensor);
	}
	
	public double getValueFahrenheit() throws SensorNotActivatedException{
		return 1.8 * super.getValue() + 32.0;
	}
}
