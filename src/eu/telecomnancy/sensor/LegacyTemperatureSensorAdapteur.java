package eu.telecomnancy.sensor;

public class LegacyTemperatureSensorAdapteur extends LegacyTemperatureSensor implements ISensor {
	private double value;
	
	public LegacyTemperatureSensorAdapteur() {
		super();
		this.value = -1;
	}

	@Override
	public void on() {
		if(!super.getStatus())
			super.onOff();
		
		this.value = super.getTemperature();
	}

	@Override
	public void off() {
		if(super.getStatus())
			super.onOff();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if(!super.getStatus())
			throw new SensorNotActivatedException("LegacySensor not activated");
		else
			this.value = super.getTemperature();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if(!super.getStatus())
			throw new SensorNotActivatedException("LegacySensor not activated");
		
		return this.value;
	}

}
