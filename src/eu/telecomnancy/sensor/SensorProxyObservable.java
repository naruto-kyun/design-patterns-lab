package eu.telecomnancy.sensor;

import java.util.Observable;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:07
 */
public class SensorProxyObservable extends Observable implements ISensor {
    private ISensor sensor;

    public SensorProxyObservable(ISensor _sensor) {
        sensor = _sensor;
    }

    @Override
    public void on() {
        sensor.on();
    }

    @Override
    public void off() {
        sensor.off();
    }

    @Override
    public boolean getStatus() {
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        sensor.update();
        setChanged();
        notifyObservers();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return sensor.getValue();
    }
}
