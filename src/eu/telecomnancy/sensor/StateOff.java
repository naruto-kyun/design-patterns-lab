package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOff implements State {

	@Override
	public boolean getState() {
		return false;
	}

	@Override
	public double getValue() {
		return 0;
	}
	
}
