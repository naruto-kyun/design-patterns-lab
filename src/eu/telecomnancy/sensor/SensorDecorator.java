package eu.telecomnancy.sensor;

public abstract class SensorDecorator implements ISensor {
	private ISensor sensor;
	
	public SensorDecorator(ISensor _sensor){
		this.sensor = _sensor;
	}
	
	@Override
	public void on() {
		sensor.on();
	}

	@Override
	public void off() {
		sensor.off();
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();

	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue();
	}

}
