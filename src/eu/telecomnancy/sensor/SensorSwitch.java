package eu.telecomnancy.sensor;

import java.util.ArrayList;
 
public class SensorSwitch {
   private ArrayList<SensorCommand> history = new ArrayList<SensorCommand>();
 
   public void storeAndExecute(SensorCommand cmd) {
      this.history.add(cmd); 
      cmd.execute();        
   }
}
