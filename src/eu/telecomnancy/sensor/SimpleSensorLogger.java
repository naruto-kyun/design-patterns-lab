package eu.telecomnancy.sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:16
 */
public class SimpleSensorLogger implements SensorLogger {
	final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
    @Override
    public void log(LogLevel level, String message) {
    	Calendar now = Calendar.getInstance();
        System.out.println(dateFormat.format(now.getTime()) + " : " + level.name() + " " + message);
    }
}
