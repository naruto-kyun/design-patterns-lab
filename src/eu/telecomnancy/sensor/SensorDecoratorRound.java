package eu.telecomnancy.sensor;

public class SensorDecoratorRound extends SensorDecorator {
	public SensorDecoratorRound(ISensor _sensor){
		super(_sensor);
	}
	
	public long getValueRound() throws SensorNotActivatedException{
		return Math.round(super.getValue());
	}
}
