package eu.telecomnancy.sensor;

public interface State {
	public boolean getState();
	public double getValue();
}
