package eu.telecomnancy.sensor;

import java.util.Random;

public class SensorState implements ISensor {
    double value;
	State state;

    public SensorState(){
    	state = new StateOff();
    }
    
    @Override
    public void on() {
        state = new StateOn();
    }

    @Override
    public void off() {
        state = new StateOff();
    }

    @Override
    public boolean getStatus() {
        return state.getState();
    }	

    @Override
    public void update() throws SensorNotActivatedException {
        if (this.getStatus())
            value = state.getValue();
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (this.getStatus())
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
