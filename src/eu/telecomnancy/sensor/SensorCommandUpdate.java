package eu.telecomnancy.sensor;

public class SensorCommandUpdate extends SensorCommand {	
	@Override
	void execute() {
		try {
			this.sensor.update();
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}
	}

}
