package eu.telecomnancy.ui;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorCommand;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.SensorSwitch;

import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConsoleCommandUI {

    private ISensor sensor;
    private Scanner console;

    public ConsoleCommandUI(ISensor sensor) {
        this.sensor = sensor;
        this.console = new Scanner(System.in);
        manageCLI();
    }

    public void manageCLI() {
    	ReadPropertyFile rp = new ReadPropertyFile();
        Properties p = null;
        try {
            p = rp.readFile("/eu/telecomnancy/commande.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        SensorSwitch sw = new SensorSwitch();
    	
        String rep = "";
        System.out.println("quit|q: quitter");
        
        for (String i : p.stringPropertyNames())
        	System.out.println(i);
        
        while (!"q".equals(rep)) {
            System.out.print(":> ");
			rep = this.console.nextLine();
			try {
				SensorCommand command = (SensorCommand) Class.forName(p.getProperty(rep)).newInstance();
				command.setSensor(sensor);
				sw.storeAndExecute(command);
			} catch(ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				System.out.println("quit|q: quitter");
			    for (String i : p.stringPropertyNames())
			    	System.out.println(i);
			}
        }
    }
}
