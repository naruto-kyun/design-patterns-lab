package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleCommandUI;

public class AppCommand {

    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensor();
        new ConsoleCommandUI(sensor);
    }

}